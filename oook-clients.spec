Name: oook-clients
Version: 2.5.2
Release: 0
Summary: All the Oook clients
Group: System/Filesystems
Packager: UIS Platforms <unix-support@ucs.cam.ac.uk>
URL: http://wiki.csx.cam.ac.uk/ucs/Oook
License: none
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildArch: noarch
Requires: oook-client-common oook-client-dump oook-client-lvm2 oook-client-xfs

# Note that oook-client-rdiff is not available because RHEL 7 doesn't
# have rdiff.

%Description
This package depends on all of the clients for the Oook backup system
run by the University of Cambridge Information Services.  Install
this package if you want to make sure you've got all the available
clients installed and don't mind having more than you need.

%Package -n oook-client-common
Summary: Oook client configuration for all systems
Group: System/Filesystems
%if 0%{?suse_version}
Prereq: pwdutils
%else
Prereq: shadow-utils
%endif
Requires: openssh /bin/bash
Conflicts: oook-client-dump < 2

%Description -n oook-client-common
This package is likely to be needed by all systems that are to be backed
up by oook.csi.cam.ac.uk.  It does not contain scripts for backing up
particular kinds of filesystem.  It is only likely to be useful within the
University of Cambridge Information Services.

%Package -n oook-client-dump
Summary: Oook client configuration for systems using dump(8)
Group: System/Filesystems
Requires: oook-client-common dump userv
%define dumpuser dump
%define dumphome /home/dump

%Description -n oook-client-dump
This package contains all the bits necessary to enable ext2/3/4 filesystems
to be backed up by oook.csi.cam.ac.uk.  It is only likely to be useful
within the University of Cambridge Information Services.

%Package -n oook-client-gnutar
Summary: Oook client script using GNU Tar
Group: System/Filesystems
Requires: oook-client-common tar userv

%Description -n oook-client-gnutar
This package contains a script to back up files and directories to
oook.csi.cam.ac.uk using GNU Tar. It doesn't depend on using any
particular filesystem, but it also can't back up any
filesystem-specific data.  It is only likely to be useful within the
University of Cambridge Information Services.

%Package -n oook-client-lvm2
Summary: Oook client script for using LVM2 snapshots
Group: System/Filesystems
Requires: oook-client-common lvm2 userv

%Description -n oook-client-lvm2
This package contains a script for automatically creating an LVM2
snapshot of a filesystem and backing that up rather than backing up
the underlying filesystem.  This allows consistent backups to be made
even when another process writes to the filesystem in the mean time.
It is only likely to be useful within the University of Cambridge
Information Services.

%Package -n oook-client-xfs
Summary: Oook client configuration for XFS file systems
Group: System/Filesystems
Requires: oook-client-common xfsdump userv

%Description -n oook-client-xfs
This package contains all the bits necessary to enable XFS filesystems to
be backed up by oook.csi.cam.ac.uk.  It is only likely to be useful within the
University of Cambridge Information Services.

%Prep
%setup

%Install
mkdir -p ${RPM_BUILD_ROOT}/usr/bin
cp oook-dump-ext2 oook-dump-xfs oook-client oook-dtrt oook-with-lvm2-snapshot \
   oook-dump-using-gnutar ${RPM_BUILD_ROOT}/usr/bin
ln -s oook-dump-ext2 ${RPM_BUILD_ROOT}/usr/bin/oook-dump-ext3
ln -s oook-dump-ext2 ${RPM_BUILD_ROOT}/usr/bin/oook-dump-ext4
mkdir -p ${RPM_BUILD_ROOT}/usr/share/man/man1
cp oook-dump-ext2.1 oook-dump-xfs.1 oook-client.1 oook-dtrt.1 \
   oook-with-lvm2-snapshot.1 oook-dump-using-gnutar.1 \
   ${RPM_BUILD_ROOT}/usr/share/man/man1
ln -s oook-dump-ext2.1 ${RPM_BUILD_ROOT}/usr/share/man/man1/oook-dump-ext3.1
ln -s oook-dump-ext2.1 ${RPM_BUILD_ROOT}/usr/share/man/man1/oook-dump-ext4.1
mkdir -p ${RPM_BUILD_ROOT}/etc/userv/services.d
cp userv/oook-flushbufs userv/oook-dump-xfs \
   userv/oook-lvm2-predump userv/oook-lvm2-postdump \
   userv/oook-dump-using-gnutar \
   ${RPM_BUILD_ROOT}/etc/userv/services.d
> ${RPM_BUILD_ROOT}/etc/dumpdates
mkdir -p ${RPM_BUILD_ROOT}%{dumphome}
mkdir ${RPM_BUILD_ROOT}%{dumphome}/.ssh
cp authorized_keys ${RPM_BUILD_ROOT}%{dumphome}/.ssh/authorized_keys
mkdir -p ${RPM_BUILD_ROOT}/etc/oook
cp fslist ${RPM_BUILD_ROOT}/etc/oook/fslist

%Pre -n oook-client-common
if ! id "%{dumpuser}" >/dev/null 2>&1; then
    groupadd -r %{dumpuser} 2>/dev/null
    useradd -r -d %{dumphome} -g %{dumpuser} -G disk -s /bin/bash %{dumpuser} \
	2>/dev/null
fi

%Post -n oook-client-dump
# We may need to translate fslist to the new format.
if [ -f %{dumphome}/fslist ]; then
    echo "Converting Oook fslist to new format"
    sed 's/$/ dump-ext2/' < /home/dump/fslist > /etc/oook/fslist
    # Make sure this doesn't get run again, but don't lose the file.
    mv /home/dump/fslist /home/dump/fslist.backup
fi

%Files
# Main package is empty.

%Files -n oook-client-common
%defattr(-,root,root)
%attr(-,%{dumpuser},%{dumpuser}) %{dumphome}
%config(noreplace) /etc/oook/fslist
/usr/bin/oook-client
/usr/bin/oook-dtrt
/usr/share/man/man1/oook-client.1.gz
/usr/share/man/man1/oook-dtrt.1.gz

%Files -n oook-client-dump
%defattr(-,root,root)
%config(noreplace) %attr(664,root,disk) /etc/dumpdates
%config /etc/userv/services.d/oook-flushbufs
/usr/bin/oook-dump-ext2
/usr/bin/oook-dump-ext3
/usr/bin/oook-dump-ext4
/usr/share/man/man1/oook-dump-ext2.1.gz
/usr/share/man/man1/oook-dump-ext3.1.gz
/usr/share/man/man1/oook-dump-ext4.1.gz

%Files -n oook-client-gnutar
%defattr(-,root,root)
%config /etc/userv/services.d/oook-dump-using-gnutar
/usr/bin/oook-dump-using-gnutar
/usr/share/man/man1/oook-dump-using-gnutar.1.gz

%Files -n oook-client-lvm2
%defattr(-,root,root)
%config /etc/userv/services.d/oook-lvm2-postdump
%config /etc/userv/services.d/oook-lvm2-predump
/usr/bin/oook-with-lvm2-snapshot
/usr/share/man/man1/oook-with-lvm2-snapshot.1.gz

%Files -n oook-client-xfs
%defattr(-,root,root)
%config /etc/userv/services.d/oook-dump-xfs
/usr/bin/oook-dump-xfs
/usr/share/man/man1/oook-dump-xfs.1.gz
