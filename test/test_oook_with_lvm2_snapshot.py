from imp import load_module, PY_SOURCE
from io import StringIO
from unittest import TestCase, main

subjectname = "oook-with-lvm2-snapshot"
with open(subjectname) as subjectfile:
    oook_with_lvm_snapshot = load_module('oook_with_lvm_snapshot', subjectfile,
                                subjectname, ('', 'r', PY_SOURCE))

systemd_escape = oook_with_lvm_snapshot.systemd_escape

class TestEscape(TestCase):
    def test_simple(self):
        self.assertEqual(systemd_escape("/home"), "home")
    def test_root(self):
        self.assertEqual(systemd_escape("///"), "-")
    def test_escape(self):
        self.assertEqual(systemd_escape("/.poot.///poot/"), r"\x2epoot.-poot")
