#! /usr/bin/python3

from imp import load_module, PY_SOURCE
from io import StringIO
from unittest import TestCase, main

subjectname = "oook-dump-xfs"
with open(subjectname) as subjectfile:
    oook_dump_xfs = load_module('oook_client', subjectfile, subjectname,
                                  ('', 'r', PY_SOURCE))

class TestValidate(TestCase):
    def test_root(self):
        oook_dump_xfs.validatetarget('/')
    def test_home(self):
        oook_dump_xfs.validatetarget('/home')
    def test_nomatch(self):
        oook_dump_xfs.validatetarget('/secret')
    def test_bad_target(self):
        with self.assertRaises(oook_dump_xfs.Error):
            oook_dump_xfs.validatetarget("/***")

class TestXFSDumpCmd(TestCase):
    def test_full(self):
        self.assertEqual(oook_dump_xfs.xfsdumpcmd('full', '/replicated'),
            ["xfsdump", "-F", "-e", "-l", "0", "-", "/replicated"])
    def test_incr(self):
        self.assertEqual(oook_dump_xfs.xfsdumpcmd('incr', '/replicated'),
            ["xfsdump", "-F", "-e", "-l", "1", "-", "/replicated"])
    def test_bogus(self):
        with self.assertRaises(oook_dump_xfs.Error):
            oook_dump_xfs.xfsdumpcmd('bogus', '/replicated')

if __name__ == '__main__':
    main()
