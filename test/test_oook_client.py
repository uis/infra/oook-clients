#! /usr/bin/python3

from imp import load_module, PY_SOURCE
from io import StringIO
from unittest import TestCase, main

subjectname = "oook-client"
with open(subjectname) as subjectfile:
    oook_client = load_module('oook_client', subjectfile, subjectname,
                                  ('', 'r', PY_SOURCE))

class TestParseCmd(TestCase):
    def test_toolong(self):
        with self.assertRaises(oook_client.Error):
            oook_client.parsecmd(StringIO("x" * 2000))
    def test_badcmd(self):
        with self.assertRaises(oook_client.Error):
            oook_client.parsecmd(StringIO("Make me a sandwich\n"))
    def test_full(self):
        self.assertEqual(oook_client.parsecmd(StringIO("full /\n")),
                         ('full', '/'))
    def test_incr(self):
        self.assertEqual(oook_client.parsecmd(StringIO("incr /home-9_2.3\n")),
                         ('incr', '/home-9_2.3'))
    def test_bad_target(self):
        with self.assertRaises(oook_client.Error):
            oook_client.parsecmd(StringIO("incr /***"))
    def test_multiline(self):
        self.assertEqual(oook_client.parsecmd(StringIO("full /\nsplund\n")),
                         ('full', '/'))
    def test_nolf(self):
        with self.assertRaises(oook_client.Error):
            oook_client.parsecmd(StringIO("full /"))
    def test_null(self):
        with self.assertRaises(oook_client.Error):
            oook_client.parsecmd(StringIO(""))

class TestHowDoIDump(TestCase):
    normal_file = ("# Oook dump configuration\n"
                   "/home\twith-lvm-snapshot dump-xfs\n"
                   "*\tdtrt\n")
    strict_file = ("/ dump-ext4\n/home dump-ext4\n")
    def test_root(self):
        self.assertEqual(oook_client.howdoidump('full', '/',
                                                StringIO(self.normal_file)),
                         ["oook-dtrt", "full", "/"])
    def test_home(self):
        self.assertEqual(oook_client.howdoidump('incr', '/home',
                                                StringIO(self.normal_file)),
                         ["oook-with-lvm-snapshot",
                              "dump-xfs", "incr", "/home"])
    def test_nomatch(self):
        with self.assertRaises(oook_client.Error):
            oook_client.howdoidump('incr', '/secret',
                                       StringIO(self.strict_file))

if __name__ == '__main__':
    main()
